#include <genome.hpp>
#include <iostream>
#include <stdexcept>

using namespace Penna;

void test_copy_constructor(Genome & parent_genome) {
    Genome child_genome(parent_genome);
    for (std::size_t age=0; age < Genome::number_of_genes; ++age) {
        if (child_genome.count_bad(age) != parent_genome.count_bad(age)) {
            throw std::logic_error("Copy constructor changed the genome.");
        }
    }
}

int main() {

    // Seed the random number generator.
    srand(42);

    // Use mutation to test the copy constructor for different parent genomes.
    Genome::set_mutation_rate(5);
    Genome test_genome;

    for (int i = 0; i < 10; ++i) {
        test_genome.mutate();
        test_copy_constructor(test_genome);
    }

    return 0;

}
