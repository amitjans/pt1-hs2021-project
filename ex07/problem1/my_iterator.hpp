#ifndef MY_ITERATOR_HPP
#define MY_ITERATOR_HPP

#include <cassert>
#include <iostream> // for debugging
#include <iterator> // for iterator category tags

/*
Forward & bidirectional iterators requirements:

Iterator:
- CopyConstructible
- CopyAssignable
- Destructible
- Supports: *a (Dereferenceable)
- Supports: ++a (Preincrementable)

Input Iterator:
- All requirements of an iterator.
- Supports: == (EqualityComparable)
- Supports: !=
- Supports: ->
- Supports: a++ (Postincrementable)

Forward Iterator:
- All requirements of an input iterator
- DefaultConstructible
- Supports expression: *a++

Bidirectional Iterator:
- All requirements of a forward iterator
- Predecrementable
- Postdecrementable
- Supports expression: *a--

*/

// my iterator
template <typename T>
class MyIterator {
  public:
    // member types
    using value_type = T; // type of values obtained when dereferencing the
                          // iterator
    using difference_type = std::size_t; // signed integer type to represent
                                         // distance between iterators
    using reference = T&; // type of reference to type of values
    using pointer = T*; // type of pointer to the type of values
    using iterator_category = std::forward_iterator_tag; // category of
                                                         // the iterator
    // using iterator_category = std::bidirectional_iterator_tag; // category of
    //                                                            // the iterator

    // TODO: constructor
    MyIterator();
    MyIterator(pointer const, pointer const, pointer const);
    // copy ctor
    MyIterator(MyIterator const&) = default;
    // copy assignment
    MyIterator& operator=(MyIterator const&) = default;
    // dtor
    ~MyIterator() { }

    // TODO: operators
    MyIterator operator++(int);
    bool operator!=(MyIterator const) const;
    reference operator*() const;
    MyIterator operator->() const;
    MyIterator& operator++();
     

  private:
    // TODO: private members
    pointer first_entry_;
    pointer last_entry_;
    pointer current_entry_;
    // TODO: private method "check" to prevent illegal operations
    bool check_();
};
template<class T>
MyIterator<T>::MyIterator() : first_entry_(nullptr), last_entry_(nullptr), current_entry_(nullptr) {}


template<typename T>
MyIterator<T>::MyIterator(pointer const begin, pointer const end, pointer const current) : first_entry_(begin), last_entry_(end), current_entry_(current) {}

template<typename T>
MyIterator<T> MyIterator<T>::operator++(int) {
  assert(check_());  // check 
  return *this.current_entry_++;  // return iterator before increasing current_entry
}

template<typename T>
MyIterator<T>& MyIterator<T>::operator++() {
  current_entry_++;
  assert(check_());  // check
  return *this;
}

template<typename T>
bool MyIterator<T>::operator!=(MyIterator const iterator) const {
  return current_entry_ != iterator.current_entry_;
}

template<typename T>
typename MyIterator<T>::reference MyIterator<T>::operator*() const {
  return *current_entry_;
}

template<typename T>
MyIterator<T> MyIterator<T>::operator->() const {
  return *this;
}

template<typename T>
bool MyIterator<T>::check_() {
  return current_entry_ > last_entry_ ? false : (current_entry_ < first_entry_ ? false : true);
}
#endif /* MY_ITERATOR_HPP */
