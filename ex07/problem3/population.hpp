/* Header file for the Population class */
#ifndef __POPULATION_H__
#define __POPULATION_H__

#include "genome.hpp"
#include "animal.hpp"
#include <list>
#include <functional>
#include <algorithm>

// Penna namespace
namespace Penna {

bool DeathPredicate(const unsigned int);  // function that kills an animal with probability 1 - input

// Population class
class Population {
  public:
    Population();  // default constructor
    Population(const age_t);  // constructor using number of animals

    void aging(void);  // age all animals
    void death(void);  // remove dead animals
    void birth(void);  // add baby animals
    static void set_pop_max(const age_t);  // set pop_max_
    age_t get_pop_num(void);

  private:
    static age_t pop_max_;  // maximum number of animals N_max
    std::list<Animal> pop_;  // list of animals
};
}  // namespace Penna

#endif  // POPULATION_HPP
