/**
 * Implementation of the Penna animal class.
 * Programming Techniques for Scientific Simulations, ETH Zürich
 */

#include "animal.hpp"
#include <cassert>
#include <iostream>

namespace Penna {

// Definition of static data members
age_t Animal::bad_threshold_;
age_t Animal::maturity_age_;
double Animal::probability_to_get_pregnant_;

void Animal::set_maturity_age( age_t r ) {
    maturity_age_ = r;
}

void Animal::set_probability_to_get_pregnant( double p ) {
    probability_to_get_pregnant_ = p;
}

void Animal::set_bad_threshold( age_t t ) {
    bad_threshold_ = t;
}

age_t Animal::get_bad_threshold() {
    return bad_threshold_;
}

Animal::Animal() : gen_(), age_(0), pregnant_(false) {
}

Animal::Animal( const Genome& gen ) : gen_(gen), age_(0), pregnant_(false) {
}

bool Animal::is_dead() const {
    return age_ > maximum_age || gen_.count_bad(age_) > bad_threshold_;
}

bool Animal::is_pregnant() const {
    return pregnant_;
}

age_t Animal::age() const {
    return age_;
}

void Animal::grow() {
  assert( !is_dead() );
  ++age_;
  if (age_ > maturity_age_ && !pregnant_) {
    if (probability_to_get_pregnant_ > drand48()) {
      pregnant_ = true;
    }
  }
}

Animal Animal::give_birth() {
    assert( pregnant_ == true );
    pregnant_ = false;
    Genome child_genome = gen_;
    // Note: this "should" return a copy such that we don't expose a freely callable mutate
    //       method but not doing it in-place is slow -> ideal use for C++11 move semantics
    child_genome.mutate();
    return Animal( child_genome );
}

} // end namespace Penna
