/* Population file */

#include "population.hpp"
#include <algorithm>
#include <random>
#include <list>
#include <iostream>

// Penna namespace
namespace Penna{

age_t Population::pop_max_;

bool DeathPredicate(const float N) {
   return drand48() < N ? true : false;
}

Population::Population() : pop_(0) {}

Population::Population(const age_t num) : pop_(num) {}

void Population::set_pop_max(const age_t pop_max) {
  pop_max_ = pop_max;
}

age_t Population::get_pop_num(void) {
  return pop_.size();
}

void Population::aging(void) {
  std::for_each(pop_.begin(), pop_.end(), [](Animal& a) {a.grow(); });
}

void Population::death(void) {
  pop_.remove_if(std::mem_fn(&Animal::is_dead));
  pop_.remove_if([this](const Animal animal) {
      const float N = (float)this->pop_.size() / this->pop_max_;
      return DeathPredicate(N); 
  });
}

void Population::birth(void) {
  std::for_each(pop_.begin(), pop_.end(), [this](Animal& a) {
      if (a.is_pregnant()) {
        Animal animal = a.give_birth();
        this->pop_.push_back(animal);
      };
  });
}
} // Penna namespace



