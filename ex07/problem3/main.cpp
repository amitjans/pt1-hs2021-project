/* MAIN FILE */

#include "animal.hpp"
#include "population.hpp"
#include "genome.hpp"
#include <iostream>

int main() {
  // Constants
  Penna::Animal::set_bad_threshold(2);  // parameter T
  Penna::Animal::set_maturity_age(8);  // parameter R
  Penna::Genome::set_mutation_rate(0);  // parameter M
  Penna::Population::set_pop_max(100000);  // parameter Nmax
  Penna::Animal::set_probability_to_get_pregnant(1); 
  static const unsigned int N0 = 450000;  // parameter N0
  static const unsigned int Tmax = 10000;  // max time
  
  // Instantiate Population class
  Penna::Population pop(N0);

  // For each time step
  for (unsigned int i = 0 ; i < Tmax ; i++) {
    std::cout << "Time: " << i << "\t Number of animals: " << pop.get_pop_num() << "\n";
    pop.aging();  // age all animals
    pop.death();  // remove dead animals
    pop.birth();  // add baby animals
  }

  return 0;
}
