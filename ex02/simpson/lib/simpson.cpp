//Simpson's integration method
#include "simpson.h"
#include <cassert>
#include <cmath>

double simpsonIntegral(double (*f)(double),
                       const double a,
                       const double b,
                       const unsigned N) {
    
    assert(b >= a);
    assert(N > 0);
    assert(f != NULL); // we make sure the pointer is not NULL

    const double binSize = (b - a)/N;
    const double C = binSize/6;

    double integral = 0;

    for (unsigned i = 0; i < N; i++) {
        integral += (f(a + i * binSize) + 4 * f(a + i * binSize + binSize/2) + f(a + (i + 1) * binSize));
    }
    return integral * C;
}
