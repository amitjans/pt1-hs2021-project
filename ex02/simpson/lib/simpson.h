/*
 * Simpson library header
 */

#ifndef SIMPSON_HPP // IMPORTANT! To avoid duplications
#define SIMPSON_HPP

double simpsonIntegral(double (*f)(double), 
                       const double a, 
                       const double b, 
                       const unsigned N);

/* PRECONDITIONS:
 *
 * the domain of the function f(x) covers the interval [min(a,b), max(a,b)]
 *
 * boundary values 'a' and 'b' are convertible to double
 *
 * 'N' convertible to unsigned
 *
 * 'N' > 0
 *
 * POSTCONDITIONS: the return value will approcimate the integarl of the
 *                 function f(x) over the interval [min(a,b), max(a,b)] by
 *                 the use of the Simpson rule with 'N' equally sized intervals
 *
 * COMPLEXITY: number of function calls = 2*'N' + 1
 *
 * DOCUMENTATION: the inerval given by the boundaries 'a' and 'b' will be
 *                divided to 'N' equally sized bins, the function 'f' will be
 *                approximated by a parabola using the function values at the
 *                bin-boundaries and in the bin-midpoint the integral will be
 *                approximated by the sum of the integrals over each bin of
 *                the corresponding interpolating parabol(x) covers the 
 *                interval [min(a,b), max(a,b)]
 *
 *                boundary values 'a' and 'b' are convertible to double
 *
 *                'N' convertible to unsigned
 *
 *                'N' > 0
 *
 * POSTCONDITIONS: the return value will approcimate the integarl of the
 *                 function f(x) over the interval [min(a,b), max(a,b)] by
 *                 the use of the Simpson rule with 'N' equally sized intervals
 *
 * COMPLEXITY: number of function calls = 2*'N' + 1
 *
 * DOCUMENTATION: the inerval given by the boundaries 'a' and 'b' will be
 *                divided to 'N' equally sized bins, the function 'f' will be
 *                approximated by a parabola using the function values at the
 *                bin-boundaries and in the bin-midpoint the integral will be
 *                approximated by the sum of the integrals over each bin of
 *                the corresponding interpolating parabola
 * */

#endif
