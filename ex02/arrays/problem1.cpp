#include <iostream>
#include "problem1.h"
#include <vector>

int main() {
    float staticArray[NMAX]; // static array
    std::vector<float> dynamicArray; // dynamic array
    int n; // amount of numbers
    int sum = 0; // sum of all numbers (for normalisation)
    std::cout << "Amount of numbers: " << std::endl;
    std::cin >> n;
    // we add the input values to the dynamic array and update the sum value
    for (int i = 0; i < n; i++) {
        int m;
        std::cout << "Insert number " << i << ":" << std::endl;
        std::cin >> m;
        dynamicArray.push_back(m);
        sum += dynamicArray[i];
    }
    // we normalise the array and output its values in reverse order
    for (int i = n - 1; i >= 0; i--) {
        dynamicArray[i] /= sum;
        std::cout << dynamicArray[i] << std::endl;
    }

    return 0;
}
