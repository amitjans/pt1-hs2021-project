/*HEADER FILE*/
#ifndef __PENNA_H__
#define __PENNA_H__

/*
  Pascal: This should really be two files.

  You're defining things instead of just declaring everything.

  Note that the type 'unsigned long' is only guaranteed to be at least 32-bit.
  See here: https://en.cppreference.com/w/cpp/language/types
  If you want a type with guaranteed fixed width, you should check here:
  https://en.cppreference.com/w/cpp/types/integer

  In your constructor, what type does '~0' have?
  What is the purpose of this constructor?

  A genome should probably be able to mutate(), and we also may want to get the
  number_of_bad_genes_up_to(unsigned short i)
*/

class Genome {
  public:
    using gentype = const unsigned long;

    static const unsigned short gene_number = 64; // number B of bits of genome

    Genome(const unsigned int i = 0) { // ctor
      gentype temp = (i > 0) ? (gentype)1 << i : 0; // set the ith bit of temp to 1
      gentype mask = ~(~0 << gene_number);
      genome_ = mask & temp; // set all bits over "gene_number" to 0
    }

    static void set_mutation_rate(unsigned short); // setter
    static void get_mutation_rate(unsigned short); // getter

    static Genome clone() const;

    private:
    gentype genome_;
    static unsigned short mutation_rate_ = 2;
}
/*
  Pascal:
  I would say anything related to the total population of the animals should not
  be stored in an individual animal (e.g. get_max_population and the like).

  I don't think something like an 'unsigned float' exists.

  An animal also has an age. You may also want to have a function that ages the
  animal.

  Why are you providing a default value to the genome of the animal? We probably
  want to be able to construct an animal from a genome.

  Why do you have all these static values, if you're then trying to change them
  all whenever an animal is constructed?
*/
class Fish {
  public:
    using intval = const unsigned int;
    using floatval = const unsigned float;

    Fish(intval T, intval N, intval R, floatval M) : threshold_(T), max_population_(N), mutation_rate_(R), reproduction_age_(M) {} // ctor

    static void set_threshold(intval); // setters
    static void set_max_population(intval);
    static void set_mutation_rate(floatval);
    static void set_reproduction_age(intval);
    static void get_threshold(intval); // getters
    static void get_max_population(intval);
    static void get_mutation_rate(floatval);
    static void get_reproduction_age(intval);

    static Fish generate_baby() const;

  private:
    Genome genome_{};
    static intval threshold_; // max number of deleterious mutations before dying
    static intval max_population_;
    static floatval mutation_rate_; // number of bits flip when child is born
    static intval reproduction_age_; // minimum age of reproduction
}
