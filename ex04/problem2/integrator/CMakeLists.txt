# add library
add_library(integratorLib ${SQUARE_LIBRARY_TYPE} simpson.cpp)

# add include directories (header files)
target_include_directories(integratorLib PUBLIC ./)

# specify install rules
install(TARGETS integratorLib
         ARCHIVE DESTINATION lib
         LIBRARY DESTINATION lib
         RUNTIME DESTINATION bin
        )
# specify install rules for the header
install(FILES simpson.hpp DESTINATION include)
