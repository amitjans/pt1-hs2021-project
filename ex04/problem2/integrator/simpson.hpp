/* Programming Techniques for Scientific Simulations, HS 2021
 * Simpson library header
 */

/*
  Pascal:
  This will only work with objects of the specific type "MyFunc". You should
  have used templates instead.

  Note that your operator() can be const.
*/

#ifndef SIMPSON_HPP // IMPORTANT! To avoid duplications.
#define SIMPSON_HPP
#include <math.h>

class MyFunc {
  public:
    MyFunc(double l = 0.) : lambda(l) {}
    double operator () (double x) {
      return std::exp(-lambda*x);
    }
  private:
    const double lambda;
};


double integrate(const double a,
                 const double b,
                 const unsigned bins,
                 MyFunc function);


/* PRECONDITIONS:

  the domain of the function function(x) covers the interval
  [min(a,b),max(a,b)]

  boundary values 'a' and 'b' are convertible to double

  'bins' convertible to unsigned

  'bins' > 0

  POSTCONDITIONS: the return value will approximate the integral of
                  the function function(x) over the interval [min(a,b),max(a,b)]
                  by the use of the Simpson rule with 'bins' equally sized
                  intervals

  COMPLEXITY: number of function calls = 2*bins+1

  DOCUMENTATION: the interval given by the boundaries 'a' and 'b' will
                 be divided to 'bins' equally sized bins, the function
                 'function' will be aproximated by a parabola using the function
                 values at the bin-boundaries and in the bin-midpoint the
                 integral will be approximated by the sum of the integrals over
                 each bin of the corresponding interpolating parabola
*/

#endif
