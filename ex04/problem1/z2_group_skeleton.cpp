/*
  Pascal: Looks good overall, well done. A few comments:

  The operators
  T operator*(const T a, const Z2 b)
  T operator*(const Z2 b, const T a)
  are symmetric. As such, you should implement them by reusing, e.g.
  template<class T>
  T operator*(const Z2 b, const T a) {
    return a * b;
  }

  There's a more efficient way to implement the power function, check the
  solutions.

  Whenever you write a template, you must specify the corresponding concepts.
*/

/* Programming Techniques for Scientific Simulations, HS 2021
 * Exercise 4.1
 */

#include <iostream>

enum Z2 { Plus, Minus };

template<class T>
T identity_element() { return T(1); }

template <>
Z2 identity_element() {return Plus; }

Z2 operator*(const Z2 a, const Z2 b) {
  if (a == b) {
    return Plus;
  }
  else {
    return Minus;
  }
}

std::ostream& operator<<(std::ostream& os, const Z2 a) {
  if (a == Plus) {
    os << "Plus";
  }
  else if (a == Minus) {
    os << "Minus";
  }
  return os;
}

template<class T>
T operator*(const T a, const Z2 b) {
  return b == Plus ? a : -a;
}

template<class T>
T operator*(const Z2 b, const T a) {
  return b == Plus ? a : -a;
}

template<class T>
T mypow(T a, const unsigned int n)  {
  T temp = identity_element<T>();
  for (unsigned int i = 0; i < n; i++) {
    temp = temp * a;
  }
  return temp;
}

int main()
{
  // some testing: feel free to add your own!
  std::cout << Plus*Plus << std::endl;
  std::cout << Plus*Minus << std::endl;
  std::cout << Plus*-1*Minus << std::endl;
  std::cout << (1.+3.)*mypow(Minus,4) << std::endl;
  for (unsigned i=0; i<7; i++)
    std::cout << "Plus^" << i << " = " << mypow(Plus,i) << std::endl;
  for (unsigned i=0; i<7; i++)
    std::cout << "Minus^" << i << " = " << mypow(Minus,i) << std::endl;
  for (unsigned i=0; i<7; i++)
    std::cout << "2^" << i << " = " << mypow(2.0,i) << std::endl;
  return 0;
}
