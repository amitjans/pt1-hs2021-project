/* Programming Techniques for Scientific Simulations, HS 2021
 * Exercise 4.2
 */

#ifndef SIMPSON_HPP
#define SIMPSON_HPP

#include <iostream>
#include <cassert>
//     - F needs to be a function or function object taking two arguments convertible from T,
//       with return value convertible to T.
// Concepts needed for type T:
//     - CopyConstructible
//     - Assignable
//     - T shall support arithmetic operations with double with result convertible to T with
//       limited relative truncation errors.

// Function simpson for 1D
template<typename F, typename T>
T simpson(const T a, const T b, const unsigned bins, const F& func)
{
  assert(bins > 0);

  const T dr = (b - a) / (2.*bins);
  T I2(0), I4(0);
  T pos = a;

  for(unsigned int i = 0; i < bins; ++i) {
    pos += dr;
    I4 += func(pos);
    pos += dr;
    I2 += func(pos);
  }

  return (func(a) + 2.*I2 + 4.*I4 - func(b)) * (dr/3.);
}

// Overloading simpson function for 2D
template<typename F, typename T>
T simpson(const T a, const T b, const T c, const T d,
          const unsigned xbins, const unsigned ybins, const F& func) {
  assert(xbins > 0);
  assert(ybins > 0);
  auto function = [&] (T x) { return simpson(c, d, ybins, 
      [&] (T y) { return func(x, y); }); 
  };
  return simpson(a, b, xbins, function);
}

#endif  // SIMPSON_HPP
