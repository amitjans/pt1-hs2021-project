/* Programming Techniques for Scientific Simulations, HS 2021
 * Exercise 4.2
 */

#include <cmath>
#include <iostream>

#include "simpson.hpp"

// f(x,y)
double Phi (const double x, const double y) {
    if ((std::pow(x, 2) + std::pow(y, 2)) <= double(1)) { return 1; }
    else { return 0; }
}

double R = 1.7606;

double Phi2 (const double x, const double y) {
  double rad = std::pow(x, 2) + std::pow(y, 2);
  if (rad <= std::pow(R, 2)) { 
    return std::exp(-rad);
  }
  else { return 0; }
}

int main() {
  const unsigned xbins=1000;
  const unsigned ybins=1000;
  const double a(-R);
  const double b(R);
  const double c(-R);
  const double d(R);
  
  std::cout.precision(15);

  // Call using function object
  std::cout << "Integral result is " << simpson(a, b, c, d, xbins, ybins, Phi2) << std::endl;

  return 0;
}
