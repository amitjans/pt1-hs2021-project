/* Programming Techniques for Scientific Simulations, HS 2021
 * Exercise 4.2
 */

#ifndef SIMPSON_HPP
#define SIMPSON_HPP

#include <cassert>

// Concepts needed for type F:
//     - F needs to be a function or function object taking a single argument convertible from T,
//       with return value convertible to T.
// Concepts needed for type T:
//     - CopyConstructible
//     - Assignable
//     - T shall support arithmetic operations with double with result convertible to T with
//       limited relative truncation errors.

// TRAITS
template<class F>
struct domain_t {
  typedef typename F::input_t type;
};

template<typename R, typename T>
struct domain_t<R(*)(T)> {
  typedef T type;
};

template<class F>
struct result_t {
  typedef typename F::output_t type;
};

template<typename R, typename T>
struct result_t<R(*)(T)> {
  typedef R type;
};

// Simpson function

template<typename F>
typename result_t<F>::type integrate(const typename domain_t<F>::type a, const typename domain_t<F>::type b, const unsigned bins, const F& func)
{
  assert(bins > 0);

  const typename domain_t<F>::type dr = (b - a) / (2.*bins);
  typename result_t<F>::type I2(0), I4(0);
  typename domain_t<F>::type pos = a;

  for(unsigned int i = 0; i < bins; ++i) {
    pos += dr;
    I4 += func(pos);
    pos += dr;
    I2 += func(pos);
  }

  return (func(a) + 2.*I2 + 4.*I4 - func(b)) * (dr/3.);
}



#endif  // SIMPSON_HPP
