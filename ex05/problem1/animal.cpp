#include <experimental/random>
#include "genome.hpp"
#include "animal.hpp"

Penna::age_t Penna::Animal::bad_threshold_ = 3;
Penna::age_t Penna::Animal::maturity_age_ = 2;
double Penna::Animal::probability_to_get_pregnant_ = 1.;

Penna::Animal::Animal() : age_(0) {}

Penna::Animal::Animal(const Penna::Genome& gen) : gen_(gen), age_(0) {}

void Penna::Animal::set_maturity_age(Penna::age_t m_age) {
  maturity_age_ = m_age;
}

void Penna::Animal::set_probability_to_get_pregnant(double p) {
  probability_to_get_pregnant_ = p;
}

void Penna::Animal::set_bad_threshold(Penna::age_t T) {
  bad_threshold_ = T;
}

Penna::age_t Penna::Animal::get_bad_threshold() {
  return bad_threshold_;
}

bool Penna::Animal::is_dead() const {
  if (age_ > maximum_age) {
    return true;
  }
  else {
    Penna::age_t count = gen_.count_bad(age_);
    if ((count > bad_threshold_) || (age_ > maximum_age)) {
      return true;
    }
  }
  return false;
}

bool Penna::Animal::is_pregnant() const {
  if (age_ >= maturity_age_) {
    int random = std::experimental::randint(0, 100);  // random number between 0 and 100
    if (random <= probability_to_get_pregnant_*100) {  
      return true;
    }
  }
  return false;
}

Penna::age_t Penna::Animal::age() const {
  return age_;
}

void Penna::Animal::grow() {
  age_ += 1;
}

Penna::Animal Penna::Animal::give_birth() {
  Penna::Animal *baby = new Penna::Animal(this->gen_);
  baby->gen_.mutate();
  return *baby;
}
