/* MAIN FILE */
#include <iostream>
#include "animal.hpp"
#include "genome.hpp"

int main() {
  Penna::Animal human;
  for (int i = 0; i < 100; i++) {
    human.grow();
  }
  bool pregnant = human.is_pregnant();
  Penna::Animal kid = human.give_birth();
  bool dead = human.is_dead();
  std::cout << "The human age is: " << human.age() << "\n";
  std::cout << "The human is pregnant? " << pregnant << "\n";
  std::cout << "The human is dead? " << dead << "\n";
  std::cout << "The kid has age " << kid.age() << "\n";
}
