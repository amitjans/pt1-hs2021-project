#include <set>
#include <bitset>
#include <cassert>
#include <iostream>
#include <experimental/random>
#include "genome.hpp"

Penna::age_t Penna::Genome::mutation_rate_ = 3;

void Penna::Genome::set_mutation_rate(Penna::age_t mrate){
  mutation_rate_ = mrate;
}

Penna::age_t Penna::Genome::count_bad(Penna::age_t N) const {
  assert(N < number_of_genes);
  std::bitset<number_of_genes> mask;
  for (Penna::age_t i = 0; i < N; i++) {
    mask.flip(i);  // flip first N bits of mask
  }
  Penna::age_t result = (Penna::age_t)(genes_ & mask).count();  // we remove all 1s after N bits and count
  return result;
}

void Penna::Genome::mutate() {
  std::set<int> bit_positions;  // store M different random positions
  unsigned int low = 0;
  unsigned int high = number_of_genes - 1;
  while (bit_positions.size() < mutation_rate_) {
    bit_positions.insert(std::experimental::randint(low, high));
  }
  std::set<int>::iterator it;  // flip all bits in the specified positions
  for (it = bit_positions.begin(); it != bit_positions.end(); it++) {
    genes_.flip(*it);
  }
}
