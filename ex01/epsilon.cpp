/*
	Pascal: Looks good to me, albeit not necessarily precise.

	Note that python's import is very different from preprocessor directives.
	In python, there is a whole import machinery, while the #include directive
	merely instructs the preprocessor to copy-paste a specific file.

	Personally, I'm not a fan of 'using namespace xxx'. At the very least, you
	pollute the global namespace and make it harder to keep track of where specific
	functions and classes are actually coming from.
*/

// "include" is like the import in Python
#include <iostream>
// namespace allow us to group named entities into narrower scopes, that otherwise
// would have global scope.
using namespace std;

void machineEpsilon(float EPS) {

	float prev_epsilon = EPS;

	while ((1+EPS) != 1){
		prev_epsilon = EPS;

		EPS /= 2;
	}

	cout << "Machine Epsilon is: " << prev_epsilon << "\n";
}

int main(){
	machineEpsilon(0.5);
}
