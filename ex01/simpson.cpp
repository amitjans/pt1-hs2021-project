/*
	Pascal: Looks good to me.

	You could still optimize the number of mathematical operations. For example,
	per call to calculateSimpsonIntegral, you only need one multiplication with
	binSize/6, as opposed to the N you are performing. You are also calling f
	more often than necessary.
*/

//Simpson's integration method
#include <iostream>
#include <cmath>

constexpr double pi(){ return std::atan(1)*4; }

double f(double x){
	return std::sin(x);
}

double calculateSimpsonIntegral(int N){
	double b = pi();
	double a = 0.;
	double binSize = (b - a)/N;
	double integral = 0;
	for (int i = 0; i < N; i++) {
		integral += binSize/6 * (f(a + i * binSize) + 4 * f(a + i * binSize + binSize/2) + f(a + (i + 1) * binSize));
	}
	return integral;
}

int main(){
	int nbins;
	std::cout << "Number of bins: ";
	std::cin >> nbins;
	double result = calculateSimpsonIntegral(nbins);
	std::cout << "The integral of sin(x) from 0 to pi is: " << result << std::endl;
	return 1;
}
