/*
  Pascal: Good, that works.

  Note however, that the appropriate solution to "bad food" is not really
  landing immediately.

  Instead, when this exception is caught, you want to alleviate the issue such
  that the program can then flow on normally.
*/

#include <iostream> // for std::cout
#include <stdexcept> // for std::runtime_error

class Plane {
    public:
        void start() {
            std::cout << "Plane started successfully." << std::endl;
        }

        void serve_food() {
            throw std::runtime_error("The food is not edible!");
        }

        void land() {
            std::cout << "Plane landed successfully." << std::endl;
        }
};

int main() {
  // TODO: Modify this code so that the plane lands,
  // but serve_food is still called.
    Plane plane;
    plane.start();
  try {
    plane.serve_food();
    plane.land();
  }
  catch (std::runtime_error e) {
    std::cout << "We catched the following error: " << e.what() << "\n";
    std::cout << "We need to land immediately! Landing..." << "\n";
    plane.land();
  }

  return 0;
}
