/*
  Pascal:
  Why do your functions have void arguments?
*/

/* Timer header file */

#ifndef __TIMER_H__
#define __TIMER_H__
#include <ctime>

class Timer {
  public:
    using time_t = double;
    using result_t = double;
    Timer();  // default ctor
    void start(void);  // start counting
    void stop(void);  // stop counting
    result_t duration(void) const;  // get number of seconds

  private:
    time_t start_time_;
    result_t duration_;
    bool running_;  // running state
};

#endif
