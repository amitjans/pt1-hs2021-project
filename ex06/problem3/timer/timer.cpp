/*
  Pascal:
  Note that std::clock does not perform to the specs asked 
  for in the exercise sheet.
*/

#include "timer.hpp"
#include <ctime>
#include <stdexcept>
#include <iostream>

// set all variables to 0 and running_ to false
Timer::Timer() : start_time_(0), duration_(0), running_(false) {}

// save starting time and set running_ to true
void Timer::start() {
  start_time_ = std::clock();
  running_ = true;
}

// throw exception when running_ is false
// compute stop time and save the difference
void Timer::stop() {
  if(!running_) {
    throw std::invalid_argument("You need to call start first");
  }
  time_t stop_time = std::clock();
  duration_ = (stop_time - start_time_)/CLOCKS_PER_SEC;
  running_ = false;
}

// return duration_ value
Timer::result_t Timer::duration() const {
  return duration_;
}
