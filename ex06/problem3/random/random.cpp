#include "random.hpp"
#include <iostream>

Generator::Generator() : seed_(0) {}  // default seed is 0

Generator::Generator(Generator::gen_t seed) : seed_(seed) {}

// reset seed value to 0
void Generator::seed() { seed_ = 0; }

// change seed value
void Generator::seed(Generator::gen_t seed) { seed_ = seed; }

typename Generator::gen_t Generator::min() {
    return 0;  // I'm not sure this is the min value...
}

typename Generator::gen_t Generator::max() {
  return m_ - 1;
}

// get next random value
typename Generator::gen_t Generator::generate() {
  seed_ = (a_*seed_ + c_) % m_;
  return seed_; 
}
