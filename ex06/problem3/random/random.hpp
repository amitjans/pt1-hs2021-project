/* Header file for random generator */

/*
  Pascal: Looks good.

  You may want to have two distinct types, one that is returned
  by generate(), and one that is used for internal storage.
  Note that the numbers stored internally can be much larger
  than what can be returned by generate().

  Why are min() and max() not const?
*/

#ifndef __RANDOM_H__
#define __RANDOM_H__

class Generator {
  public:
    using gen_t = unsigned long long int;
    Generator();  // construct with default seed
    Generator(gen_t);  // construct with user-defined seed
    void seed();  // restore default seed
    void seed(gen_t);  // set seed
    gen_t min();  // smallest number generated
    gen_t max();  // largest number generated
    gen_t generate();  // get next random number
  private:
    const gen_t m_ = 1ULL << 32;  // 2^(32)
    const gen_t a_ = 1664525;
    const gen_t c_ = 1013904223;
    gen_t seed_;
};

#endif
