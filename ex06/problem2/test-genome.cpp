/*
  Pascal: Looks good.

  Except that you're using the index boundaries of the genome inconsistently.
  Once you're calling count_bad(number_of_genes), then in the other test you
  only go up to number_of_genes-1.

  You may also want to test more different genomes, in a loop maybe.
*/

/* Test that the genome is initialized with only good genes by default */

#include "genome.hpp"
#include <stdexcept>
#include <cassert>
#include <iostream>

void test(int M) {
  Penna::Genome::set_mutation_rate(M);
  Penna::Genome child_g;
  // Check if the genome has only good genes
  if(child_g.count_bad(child_g.number_of_genes) != 0) {
    throw std::logic_error("The genome is initialized with bad genes");
  }
  child_g.mutate();
  Penna::Genome parent_g(child_g);
  // Check if the parent and the child have the same number of genes
  assert(child_g.number_of_genes == parent_g.number_of_genes);
  // Check if the parent and the child have the exact same genome
  for(Penna::age_t i = 0; i < child_g.number_of_genes; i++) {
    if(child_g.count_bad(i) != parent_g.count_bad(i)) {
      throw std::logic_error("The parent genome is different from the child genome");
    }
  }
}


int main() {
  test(5);
  return 0;
}
