/*
 * Programming Techniques for Scientific Simulations I
 * HS 2021
 * Exercise 1.5
 */

#include <iostream> // for std::cout
#include <iomanip>  // for std::setprecision
#include <limits>   // for std::numeric_limits

/*
  Pascal:
  Yes, I think your assessment is correct.
  However, note that the two definitions you quote are identical.
*/


/*
The bisection method gets the true value of epsilon as
defined in the lecture. Note that this is not the same
as std::numeric_limits<double>::epsilon(). The reason
for this is that we define epsilon as the smallest
number such that 1 + epsilon != 1, whereas numeric_limits
uses the definition that the smallest number larger than
1 is 1 + epsilon. This is different because in our definition
1 + epsilon doesn't have to be representible as a double,
it just has to be rounded (up) to a value other than 1.
*/

template <typename epsilon_t>
epsilon_t epsilon_bisection() {
  const epsilon_t one  = 1;
  const epsilon_t half = 0.5;
  epsilon_t lower = 0;
  epsilon_t upper = 1;
  epsilon_t average = 1;
  epsilon_t average_old = 0;

  while (average_old != average) {
    average_old = average;
    average = half * (upper + lower);
    if (one + average == one) {
      lower = average;
    } else {
      upper = average;
    }
  }
  return upper;
}
template <typename epsilon_t>
epsilon_t epsilon_limits() {
  return std::numeric_limits<epsilon_t>::epsilon();
}


int main() {
  // Note that there are different definitions of machine precision
  // see https://en.wikipedia.org/wiki/Machine_epsilon#Variant_definitions
  // in particular, the one presented in the lecture is different from the one in the C++ standard
  std::cout << std::setprecision(20) // number of digits printed
            << "-----------------------------" << std::endl
            << "COMPUTED VALUES:" << std::endl
            << "-----------------------------" << std::endl
            << "Float:   " << epsilon_bisection<float>()   << '\n'
            << "Double:   " << epsilon_bisection<double>()   << '\n'
            << "Long double:   " << epsilon_bisection<long double>()   << '\n'
            << "-----------------------------" << std::endl
            << "THEORETICAL VALUES:" << std::endl
            << "-----------------------------" << std::endl
            << "Float:      " << epsilon_limits<float>()      << '\n'
            << "Double:      " << epsilon_limits<double>()      << '\n'
            << "Long double:      " << epsilon_limits<long double>()      << '\n';
}
