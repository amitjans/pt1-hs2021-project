/* Pascal:
 * Looks good.
 *
 * Why do you use this C-style cast to cast the value 
 * in the enum definition?
 * If your hope is that this changes the underlying 
 * type, I think you're going to be disappointed.
 * See: https://en.cppreference.com/w/cpp/language/enum
 */

/* Programming Techniques for Scientific Simulation
 *
 * Exercise 11 - Problem 2
 *
 * */

#include <iostream>

// Factorial template
template<int unsigned long long N>
struct Factorial{
  enum { value =N * Factorial<N-1>::value};
};

// Factorial end
template<>
struct Factorial<0>{
  enum { value = 1 };
};

// Binomial template
template<int unsigned N, int unsigned K>
struct Binomial{
  enum { value = Binomial<N-1, K-1>::value + Binomial<N-1, K>::value };
};

// Binomial boundary
template<int unsigned N>
struct Binomial<N, 0>{
  enum { value = 1 };
};

// Binomial boundary
template<int unsigned N>
struct Binomial<N, N>{
  enum { value = 1 };
};

int main() {
  int unsigned const N = 20;
  int unsigned const N2 = 40;
  int unsigned const K = 9;
  std::cout << "The factorial of " << N << " is: " << Factorial<N>::value << "\n";
  std::cout << "The binomial of (" << N2 << ", " << K << ") is: " << Binomial<N2, K>::value << "\n";
  return 1;
}
