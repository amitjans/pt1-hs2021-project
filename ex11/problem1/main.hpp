/* Pascal:
 * Looks like it works.
 *
 * Concerning your declaration of the fortran function, 
 * I would advise you to not declare OUT arguments as 
 * const (as you did e.g. with the INFO). 
 * This can give you very nasty behaviour.
 */

/* HEADER FILE */

#ifndef __MAIN_H__
#define __MAIN_H__

#include <vector>
#include <cmath>
#include <numbers>

using value_t = double;
using matrix_t = std::vector<value_t>;

matrix_t buildMatrix(
  const unsigned int N,
  const double K,
  const double m
  ) {
  // Builds a matrix of size N*N where the
  // diagonal values are 2 and the values next 
  // to the diagonal are -1
  matrix_t M(N * N);
  double tmp = K/m;
  for (unsigned int j = 0; j < N; j++) {
    for (unsigned int i = 0; i < N; i++) {
      if (i == j) { M[i + j * N] = 2*tmp; }
      else if ((j == i + 1) | (j == i - 1)) { M[i + j * N] = -tmp; }
    }
  }
  return M;
}

matrix_t computeEV(int const unsigned N, double const K, double const m) {
  matrix_t ev(N);
  for (int unsigned i = 1; i <= N; i++) {
    ev[i-1] = std::sqrt((K/m)*(2 - 2*std::cos((M_PI * i)/(N + 1))));
  }
  return ev;
}

extern "C" double dsyev_(
    char const   & JOBZ,
    char const   & UPLO,
    int const    & N,
    double       * A,
    int const    & LDA,
    double       * W,
    double       * WORK,
    int const    & LWORK,
    int          & INFO
);


#endif
