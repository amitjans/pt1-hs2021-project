/* 
 * Programming techniques for scientific simulation: Problem 11.1
 * 
 * */

#include <iostream>
#include <fstream>
#include <cmath>
#include "main.hpp"

int main() {
  // Open file
  std::ofstream myfile;
  // Initialize inputs to LAPACK function
  int const N = 10;
  int const K = 2;
  int const m = 4;
  matrix_t A = buildMatrix(N, K, m);
  double w[N];
  double wkopt;
  int lwork = -1;
  int info;
  // Call function to get optimal lwork (written inside wkopt)
  dsyev_('V', 'U', N, A.data(), N, w, &wkopt, lwork, info);
  lwork = (int)wkopt;  // cast value to integer
  matrix_t work(lwork);  // create array with length lwork
  dsyev_('V', 'U', N, A.data(), N, w, work.data(), lwork, info);
  // Compute theoretical eigenvalues
  matrix_t ev = computeEV(N, K, m);
  // Open file
  myfile.open("results.txt");
  // Print results
  if (info  > 0) {
    myfile << "The algorithm failed to compute eigenvalues\n";
  }
  else if (info < 0) {
    myfile << "The input " << info << " has an illegal value\n";
  }
  myfile << "The eigenvalues are: \n";
  for (int i = 0; i < N; i++) {
    myfile << std::sqrt(w[i]) << "  ";
  }
  myfile << "\n-----------------------------------\n";
  myfile << "The theoretical eigenvalues are: \n";
  for (int i = 0; i < N; i++) {
    myfile << ev[i] << "  ";
  }
  myfile << "\n-----------------------------------\n";
  myfile << "The eigenvectors are: \n";
  for (int j = 0; j < N; j++) {
    myfile << "(  ";
    for (int i = 0; i < N; i++) {
      myfile << A[i + j*N] << "  ";
    }
    myfile << ")\n";
  }
  myfile.close();
  return 0;
} 
