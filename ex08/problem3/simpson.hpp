/* File containing all the different integration methods */

#include <cassert>

double proceduralIntegrate(const double a, const double b, const unsigned int nbins, double f(double)) {
  double const dxhalf = (b - a) / (2 * nbins);
  double I = f(a);
  for (unsigned int i = 1; i < 2 * nbins; ++i) {
    I += 2. * (1.0 + i%2) * f(a + dxhalf * i);
  }
  I += f(b);

  return I*dxhalf/3.;
}

// Integrate using function pointer
double modularIntegrate(const double a,
                 const double b,
                 const unsigned bins,
                 double (*function) (double)) {

  assert(bins > 0);
  assert(function != nullptr); // make sure that the pointer is not NULL

  const unsigned int steps = 2*bins + 1;

  const double dr = (b - a) / (steps - 1);

  double I = function(a);

  for(unsigned int i = 1; i < steps-1; ++i)
    I += 2 * (1.0 + i%2) * function(a + dr * i);

  I += function(b);

  return I * (1./3) * dr;

}

// Integrate using templated function object
template<typename F, typename T>
T genericIntegrate(const T a, const T b, const unsigned bins, const F& func)
{
  assert(bins > 0);

  const T dr = (b - a) / (2.*bins);
  T I2(0), I4(0);
  T pos = a;

  for(unsigned int i = 0; i < bins; ++i) {
    pos += dr;
    I4 += func(pos);
    pos += dr;
    I2 += func(pos);
  }

  return (func(a) + 2.*I2 + 4.*I4 - func(b)) * (dr/3.);
}
