/* File containing all the functions */

#include <cmath>

// Hard-coded functions
double f1(const double x) {
  return 0;
}

double f2(const double x) {
  return 1;
}

double f3(const double x) {
  return x;
}

double f4(const double x) {
  return x*x;
}

double f5(const double x) {
  return std::sin(x);
}

double f6(const double x) {
  return std::sin(5*x);
}

// Abstract Base Class
class SimpleFunction {
  public:
    using output_t = double;
    using input_t = double;
    virtual output_t operator()(const input_t&) const = 0;
};

class F1 : public SimpleFunction {
  public:
    output_t operator()(const input_t& x) const override { return 0; }
};

class F2 : public SimpleFunction {
  public:
    output_t operator()(const input_t& x) const override { return 1; }
};

class F3 : public SimpleFunction {
  public:
    output_t operator()(const input_t& x) const override { return x; }
};

class F4 : public SimpleFunction {
  public:
    output_t operator()(const input_t& x) const override { return x*x; }
};

class F5 : public SimpleFunction {
  public:
    output_t operator()(const input_t& x) const override { return std::sin(x); }
};

class F6 : public SimpleFunction {
  public:
    output_t operator()(const input_t& x) const override { return std::sin(5*x); }
};
