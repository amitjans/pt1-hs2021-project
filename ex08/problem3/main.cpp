/* MAIN FILE:
 *
 * Benchmark of Simpson integrations
 *
 * */

#include "functions.hpp"
#include "simpson.hpp"
#include <chrono>
#include <array>
#include <iostream>
#include <iomanip>  // for std::setw, std::setprecision

// Template function that measures the time of integrating "func" repeated by "num_ops" times
template<class I, class F>
int measure_integrator(const size_t & num_ops, I & integrator, const double a, 
                       const double b, const unsigned N, const F& func) {
  auto start = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
  for (size_t i = 0; i < num_ops; i++) {
    (*integrator)(a, b, N, func);
  }
  auto end = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
  return (end - start) / num_ops;
}

// Define another function for the case when the integration function is a template
// (I don't know how to generalize it for this case)
template<class F>
int measure_integrator(const size_t & num_ops, const double a, 
                       const double b, const unsigned N, const F& func) {
  auto start = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
  for (size_t i = 0; i < num_ops; i++) {
    genericIntegrate(a, b, N, func);
  }
  auto end = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
  return (end - start) / num_ops;
}
int main() {
  // Constants
  const size_t num_ops = 1e5;
  const double a = 0.;
  const double b = 1.;
  const unsigned int N = 100;
  // Instantiate functions from ABC
  F1 virtual_f1;
  F2 virtual_f2;
  F3 virtual_f3;
  F4 virtual_f4;
  F4 virtual_f5;
  F6 virtual_f6;
  std::cout
    << std::right << "# "
    << std::setw(6) << "F" << ' '
    << std::setw(13) << "Procedural[ns/op]" << ' '
    << std::setw(13) << "Modular[ns/op]"   << ' '
    << std::setw(13) << "Generic[ns/op]"    << ' '
    << std::setw(13) << "O.O.[ns/op]" << '\n'
  ;
  // Create array of function pointers to the "hard-coded" functions
  typedef double (*intFunction) (double x);
  intFunction functions[] = {f1, f2, f3, f4, f5, f6}; 
  // Create array of ABC function pointers
  std::array<SimpleFunction*, 6> virtual_functions = {&virtual_f1, &virtual_f2,  &virtual_f3, &virtual_f4, &virtual_f5, &virtual_f6};
  // Iterate over the two arrays 
  for(unsigned i = 0; i < 6; i++){
    std::cout
        << std::right << std::fixed << std::setprecision(6) << "  "
        << std::setw(6) << "f" << i << ' '
        << std::setw(13)
        << measure_integrator(num_ops, proceduralIntegrate, a, b, N, *functions[i]) << ' '
        << std::setw(13)
        << measure_integrator(num_ops, modularIntegrate, a, b, N, functions[i]) << ' '
        << std::setw(13) 
        << measure_integrator(num_ops, a, b, N, functions[i]) << ' '
        << std::setw(13) 
        << measure_integrator(num_ops, a, b, N, *virtual_functions[i]) << '\n'
    ;
  }
  
  return 0;
}

