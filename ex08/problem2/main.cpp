/* Programming Techniques for Scientific Simulations, HS 2021
 * Exercise 4.2
 */

#include "simpson.hpp"
#include <cmath>
#include <iostream>
#include <array>

int main() {
  // Constants
  const unsigned xbins(1000);
  const double a(0.);
  const double b(1.);
  // Instantiate the two functions
  MyFunc1 f1;
  MyFunc2 f2;
  
  std::cout.precision(15);
  // With an ABC we can create an array of functions an iterate over it! 
  std::array<SimpleFunction *, 2> f_arr = {&f1, &f2};

  // Iterate over the array of function pointers 
  for (auto f: f_arr) { 
    std::cout << "I =  " << simpson(a, b, xbins, *f) << std::endl;
  }
  return 0;
}
