/* Programming Techniques for Scientific Simulations, HS 2021
 * Exercise 4.2
 */

#ifndef SIMPSON_HPP
#define SIMPSON_HPP

#include <iostream>
#include <cassert>
#include <cmath>
//     - F needs to be a function or function object taking two arguments convertible from T,
//       with return value convertible to T.
// Concepts needed for type T:
//     - CopyConstructible
//     - Assignable
//     - T shall support arithmetic operations with double with result convertible to T with
//       limited relative truncation errors.

// Abstract Base Class for functions
class SimpleFunction {
  public:
    using output_t = double;
    using input_t = double;
    virtual output_t operator()(const input_t&) const = 0;
};

// Derived class
class MyFunc1 : public SimpleFunction {
  public:
    output_t operator()(const input_t& x) const override { return x*std::sin(x); }
};

// Derived class
class MyFunc2 : public SimpleFunction {
  public:
    output_t operator()(const input_t& x) const override { return x*x*std::sin(x); }
};

// Function simpson for 1D
template<typename F, typename T>
T simpson(const T a, const T b, const unsigned bins, const F& func)
{
  assert(bins > 0);

  const T dr = (b - a) / (2.*bins);
  T I2(0), I4(0);
  T pos = a;

  for(unsigned int i = 0; i < bins; ++i) {
    pos += dr;
    I4 += func(pos);
    pos += dr;
    I2 += func(pos);
  }

  return (func(a) + 2.*I2 + 4.*I4 - func(b)) * (dr/3.);
}
#endif  // SIMPSON_HPP
